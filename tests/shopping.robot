*** Settings ***
Library           AppiumLibrary
Library           Process

*** Variables ***
${APPIUM_SERVER}   http://172.28.144.1:4444
${URL}      https://www.celestino.gr/
${ACCEPT_COOKIES}       xpath=//*[@id="CybotCookiebotDialogBodyButtonAccept"]
${CHOOSE_CATEGORY}      xpath=//*[@id="ContentPlaceHolder1_link2"]
${CHOOSE_ITEM}      xpath=//*[@id="bkn2"]/div[1]/a[1]/img
${CHOOSE_COLOR}     xpath=//*[@id="ColorPanel"]/div[1]/a/img
${ADD_TO_CART}     xpath=//*[@id="ContentPlaceHolder1_CartAddMobile"]
${CART}     xpath=//*[@id="cartlink"]
${ADD_AMOUNT}       xpath=//*[@id="mypls0"]
${CHECKOUT_BUTTON}      xpath=//*[@id="GoCheckoutButton"]
${NAME_FIELD}       xpath=//*[@id="ContentPlaceHolder1_LoginUser_UserName"]
${NAME}     emilypap97s@gmail.com
${PASSWORD_FIELD}        xpath=//*[@id="ContentPlaceHolder1_LoginUser_Password"]
${PASSWORD}     pass123
${LOGIN_BUTTON}     xpath=//*[@id="ContentPlaceHolder1_LoginUser_LoginButton"]

*** Test Cases ***
Sweater purchase test

    Open Chrome
    Accept Cookies
    Select Category
    Select Item
    Select Color
    Go to Shopping Cart
    Add Amount
    Go To Checkout
    Register
    Close Browser



*** Keywords ***
Open Chrome
    Open Application    ${APPIUM_SERVER}     platformName=android        platformVersion=11      udid=emulator-5554      automationName=uiautomator2     browserName=crome
    Go To Url         ${URL}


Accept Cookies
    Wait Until Page Contains Element      ${ACCEPT_COOKIES}
    Click Element       ${ACCEPT_COOKIES}

Select Category
    Wait Until Page Contains Element      ${CHOOSE_CATEGORY}
    Click Element       ${CHOOSE_CATEGORY}

Select Item
    Wait Until Page Contains Element        ${CHOOSE_ITEM}
    Click Element       ${CHOOSE_ITEM}

Select Color
    Wait Until Element Is Visible       ${CHOOSE_COLOR}
    Click Element       ${CHOOSE_COLOR}
    Click Element       ${ADD_TO_CART}

Go to Shopping Cart
    Wait Until Page Contains Element       ${CART}
    Click Element   ${CART}

Add Amount
   Wait Until Page Contains Element     ${ADD_AMOUNT}
   Click Element  ${ADD_AMOUNT}

Go to Checkout
    Wait Until Page Contains Element   ${CHECKOUT_BUTTON}
    Click Element       ${CHECKOUT_BUTTON}

Register
    Wait Until Element Is Visible   ${NAME_FIELD}
    Input Text      ${NAME_FIELD}       ${NAME}
    Input Text      ${PASSWORD_FIELD}       ${PASSWORD}
    Click Element       ${LOGIN_BUTTON}
